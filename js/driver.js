angular.module('theDriver', []).controller('mainController', function($scope, $http, $window) {

    //-----------------------------------------------------
    //---------------------VARIABLES-----------------------
    //-----------------------------------------------------

    var audio = { //audio stuff
        "playing": false,
        "player": new Audio("resources/sprach.mp3")
    };

    // all data relating to the sim goes in here:
    $scope.simulation = {
        "bodies": [],
        "properties": {
            "running": false, //if true, do calculations every 10ms and update the vis.
        },
        "worker": new Worker("js/simulation.js"),
    }

    // this is for the dropdown menu default value.
    $scope.loadfile ="planets.txt"
    var bodycount = 0; //count bodies when creating them
    /**
     * Load data into simulation file
     * @param  {string} fname the file name to read from
     */
    $scope.loadData = function(fname) {
        $http.get("data/" + fname).success(function (data) {
            var lines = data.split('\n');

            console.log("num bodies: " + lines[0]);
            $scope.simulation.properties.orbital_radius = lines[1];
            var bodies = [];
            for (var i = 2; i < lines.length; i++) {
                if (lines[i] !== "") {
                    bodies.push(createbody(lines[i]));
                }
            }

            //build simulation worker
            $scope.simulation.worker.postMessage({
                "msg": "setData",
                "data": bodies
            });

            //update visualization
            $scope.$broadcast("updateVisualization", null)


        }).error(function (error, status) {
            //if there was a problem...
            console.error(status, error);
            alert("cannot load data: data/" + fname);
        });
    }

    /**
     * Progamatically shade a color (make it darker)
     * source: https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
     * @param  {string} color   hex format color string
     * @param  {double} percent how much darker to make it
     * @return {string}         darker color in hex format
     */
     function shadeColor(color, percent) {
         var f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
         return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
     }

    /**
     * Create and return a body object from the string body definition from a file
     * @param  {string} bodydef a string in format x, y, x-velocity, y-velocity, mass
     * @return {object}         the body object
     */
    var createbody = function(bodydef) {
        var barray = bodydef.split(" ");
        console.log("creating body", barray);
        var body = {
            "x": Number(barray[0]),
            "y": Number(barray[1]),
            "vx": Number(barray[2]),
            "vy": Number(barray[3]),
            "mass": Number(barray[4]),
            "fx": 0,
            "fy": 0,
            "id": bodycount++, //body uid
            "enabled": true, //is this body "enabled" for the simulation?
	        "expanded": false //is the data view for this body hidden?
        }
        return body;
    }

	$scope.buttonstyle = function(body) {
        // console.log("bodystyle", body.expanded, color);
        if($scope.simulation.properties.running) return {"background": "rgb(199, 199, 199)"};
        else return body.expanded ? {"background": shadeColor(color[body.id], -0.50)} : {"background": color[body.id]}
    }


    //--------------------------------------------------
    //--------------simulation controls-----------------
    //--------------------------------------------------

    $scope.startSimulation = function() {
        $scope.simulation.worker.postMessage({"msg": "start"});
        $scope.simulation.properties.running = true;
        audio.player.play();
    }

    $scope.pauseSimulation = function() {
        $scope.simulation.worker.postMessage({"msg": "pause"});
        $scope.simulation.properties.running = false;
        audio.player.pause();
    }

    $scope.stepSimulation = function() {
        $scope.simulation.worker.postMessage({"msg": "step"});
    }

    //when the simulation worker updates, do this:
    $scope.simulation.worker.onmessage = function(e) {
        if (e.data.msg === "newBodies") {
            // console.log("replacing bodies with new ones from simulation worker", e.data.data);
            $scope.simulation.bodies = e.data.data;
            $scope.$apply();
            $scope.$broadcast("updateVisualization", null)
        }
    }

    $scope.updateData = function() {
        if ($scope.simulation.running) console.error("don't update data when simulation is running!");
        $scope.simulation.worker.postMessage({
            "msg": "setData",
            "data": $scope.simulation.bodies
        });
    }


});
