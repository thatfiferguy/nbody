console.log("Simulation worker has been born. Hello world!");
var bodies = [];
var running = false;
const G = Number(6.67e-11); //approx gravitational constant
const TIMEINCREMENT = 50000; //deltaT
setInterval(function() { if (running) simulationStep(); }, 25); //do a simulation step every ___ ms

var simulationStep = function() {
    console.log("step");
    //calculate forces
    for (var i = 0; i < bodies.length; i++) { //for each body
        if (!bodies[i].enabled) continue; //skip disabled bodies
        bodies[i].fx = 0;
        bodies[i].fy = 0;
        for (var j = 0; j < bodies.length; j++) { //forces on each other body
            if (i != j && bodies[j].enabled) { //but don't apply to self, or bodies whicih are disabled
                var r = Math.sqrt(Math.pow((bodies[j].x - bodies[i].x), 2)
                                + Math.pow((bodies[j].y - bodies[i].y), 2));
                var f = (G * bodies[i].mass * bodies[j].mass)/Math.pow(r, 2);
                bodies[i].fx = bodies[i].fx + f * ((bodies[j].x - bodies[i].x)/r);
                bodies[i].fy = bodies[i].fy + f * ((bodies[j].y - bodies[i].y)/r);
            }
        }
    }
    //calculate new velocities and positions
    for (var i = 0; i < bodies.length; i++) { //for each body
        if (!bodies[i].enabled) continue; //skip disabled bodies
        //acceleration
        var ax = bodies[i].fx/bodies[i].mass;
        var ay = bodies[i].fy/bodies[i].mass;
        //velocity
        bodies[i].vx = bodies[i].vx + TIMEINCREMENT * ax;
        bodies[i].vy = bodies[i].vy + TIMEINCREMENT * ay;
        //position
        bodies[i].x = bodies[i].x + TIMEINCREMENT * bodies[i].vx;
        bodies[i].y = bodies[i].y + TIMEINCREMENT * bodies[i].vy;
    }
    //done with calculations, push bodies up to master
    postBodies();
}

//post bodies back up to the driver
var postBodies = function() {
    postMessage({
        "msg": "newBodies",
        "data": bodies
    });
}

onmessage = function(e) {
    if (e.data.msg === "start") {
        console.log("starting simulation");
        running = true;
    } else if (e.data.msg === "pause") {
        console.log("pausing simulation");
        running = false
    } else if (e.data.msg === "step") {
        simulationStep();
        console.log("simulation step");
    } else if (e.data.msg === "setData") {
        if (running) {
            console.error("simulation is running but user tried to edit data!");
            alert("setData called in simulation while running. Don't do this!");
            return;
        }
        bodies = e.data.data;
        console.log("worker: initialized bodies:", bodies);
        postBodies()
    } /* else if (e.data === "getBodies") {
        console.log("worker: retrieving bodies");
        postMessage({
            "msg": "newBodies",
            "data": bodies
        });
    } */
}
