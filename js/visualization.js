angular.module("theDriver").component("visualization",{
    template: '<span id="visualizationreplace">this will be replaced with the visualization...</span>',
        bindings: {
            simulation: '=' //the data
        }, //refer to with $scope.$ctrl.simulation
    restrict: "E",
    controller: function ($scope, $window) {
        var iterationcount = 0;
        var buildSVG = function() {
            console.log("building visualization, iteration", iterationcount++);

            var margin = {top: 40, right: 40, bottom: 60, left: 80},
            width = 500 - margin.left - margin.right,
            height = 500 - margin.top - margin.bottom;

            var x = d3.scale.linear().range([0, width]);

            var y = d3.scale.linear().range([height, 0]);

            var color = d3.scale.category20();

            var xAxis = d3.svg.axis()
            .scale(x)
            .tickSize(1)
            .ticks(2) //number of markings
						.tickFormat(d3.format(".0e"))
            .orient("bottom");

            var yAxis = d3.svg.axis()
            .scale(y)
            .tickSize(1)
            .ticks(2) //number of markings
						.tickFormat(d3.format(".0e"))
            .orient("left");

            // remove previous SVG or content
            document.getElementById("visualizationreplace").innerHTML = "";

            var svg = d3.select("#visualizationreplace").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            // get new copy of data
            var data = $scope.$ctrl.simulation.bodies;
            xValue = function(d) { return d.x; };
            yValue = function(d) { return d.y; };

            x.domain([Math.min(d3.min(data, xValue)-1, -$scope.$ctrl.simulation.properties.orbital_radius), Math.max(d3.max(data, xValue)+1, $scope.$ctrl.simulation.properties.orbital_radius)]).nice();
            y.domain([Math.min(d3.min(data, yValue)-1, -$scope.$ctrl.simulation.properties.orbital_radius), Math.max(d3.max(data, yValue)+1, $scope.$ctrl.simulation.properties.orbital_radius)]).nice();

            // x.domain([d3.min(data, xValue)-1, d3.max(data, xValue)+1]).nice();
            // y.domain([d3.min(data, yValue)-1, d3.max(data, yValue)+1]).nice();

            // orbit radius
            // svg.append("circle")
            // .attr("cx", x(0))
            // .attr("cy", y(0))
            // .attr("r", Math.max(y($scope.$ctrl.simulation.properties.orbital_radius), x($scope.$ctrl.simulation.properties.orbital_radius)))
            // .attr("fill", "white")
            // .attr("stroke", "black")
            // .attr("stroke-width", "1");



            svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
            .append("text")
            .attr("class", "label")
            .attr("x", width)
            .attr("y", -6)
            .style("text-anchor", "end")
            .text(function(d) { return "x"; }); // label


            svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .append("text")
            .attr("class", "label")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text(function(d) { return "y"; }); //label

            svg.selectAll(".otherDot")
            .data($scope.$ctrl.simulation.bodies)
            .enter().append("circle")
            .attr("class", "dot")
            .attr("r", 3.5)
            .attr("cx", function(d) { return x(d.x); })
            .attr("cy", function(d) { return y(d.y); })
            .style("fill", function(d) { return d.enabled ? $window.color[d.id] : "#b0b0b0"; });

            // legend stuff:
            // var legend = svg.selectAll(".legend")
            // .data(color.domain())
            // .enter().append("g")
            // .attr("class", "legend")
            // .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });
            //
            // legend.append("rect")
            // .attr("x", width - 18)
            // .attr("width", 18)
            // .attr("height", 18)
            // .style("fill", color);
            //
            // legend.append("text")
            // .attr("x", width - 24)
            // .attr("y", 9)
            // .attr("dy", ".35em")
            // .style("text-anchor", "end")
            // .text(function(d) { return d; });

            //append rect to svg so that we can click on it
            svg.append("rect")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .attr("fill", "rgba(255, 0, 0, 0)"); //transparent so that we can see it, but not "none" because that barrs clicking on it

            $scope.$on("redrawData", function() {
                var data = $scope.$ctrl.simulation;
                svg.selectAll(".dot")
                .attr("cx", function(d) { return x(d[x]); })
                .attr("cy", function(d) { return y(d[y]); });
                // .style("fill", function(d) { return color(d.cluster); });
            });
        };
        //listen for broadcast telling visualizations to update (/build)
        $scope.$on("updateVisualization", function() {
            buildSVG();
        });
    }
});
