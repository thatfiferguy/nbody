# NBody
NBody is a simulation of a set of particles in a 2d plane. These particles are affected by gravitational forces. This is a joint project between **Ella Tuson** and **Josh Trahan** as part of Clark University's **ASTR002: Planets and Space Exploration** class.
## Installation
1. Clone this repo to some folder
2. Run server.sh (`sh server.sh`) to start a local python http server to host files -- we use AngularJs' *$http* service to retrieve our data, and therefore js functionality requires the site to be hosted from an http server.
3. Open a browser and navigate to 	[http://localhost:31415/](http://localhost:31415/).

## Credits
* [Clark University's CS121 NBody assignment](http://cs.clarku.edu/~cs121/nbody.html) for physics overview.
* [Princeton University's NBody directory](ftp://ftp.cs.princeton.edu/pub/cs126/nbody/) for datasets.
